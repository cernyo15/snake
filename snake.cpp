#include <iostream>
#include <ncurses.h>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

using namespace std;

//Global game variables
bool gameOver = false;
bool gamePause = false;
bool win = false;
bool walls = false;
bool bariers = false;
int width = 40;
int height = 40;
int dir = 0; //Direction wariable (0=Stop, 1=Left, 2=Right, 3=Up, 4=Down)
int x, y, fruitX, fruitY, score, nTail;
int tailX[2000], tailY[2000];

pair<int, int> ran() //Function for generate two random numbers
{
    //New seed for random
    srand(static_cast<unsigned>(time(0)));

    //Generate two random numbers betwen 20 and 40
    int width = rand() % 21 + 20;
    int height = rand() % 21 + 20;

    //Create a pair for returning two numbers
    pair<int, int> result(width, height);

    return result;
}

void Setup() {
    gameOver = false;
    dir = 0;
    score = 0;
    x = rand() % (width - 2) + 1;
    y = rand() % (height - 2) + 1;

    while (y == height/2)
    {
        y = rand() % (height - 2) + 1;
    }

    //Avoid spawn in wall
    fruitX = rand() % (width - 2) + 1; 
    fruitY = rand() % (height - 2) + 1; 

    while(fruitX == x && fruitY == y)
    {
        fruitX = rand() % (width - 2) + 1; 
        fruitY = rand() % (height - 2) + 1;
    }
}

void Draw() {
    clear(); //Clear console

    //Terminal dimensions
    int maxX, maxY;
    getmaxyx(stdscr, maxY, maxX);

    //Center position for start
    int startX = (maxX - width) / 2;
    int startY = (maxY - height) / 2;

    //Draw the game area
    for (int i = 0; i < height + 2; i++) 
    {
        for (int j = 0; j < width + 2; j++) 
        {
            if (i == 0 || i == height + 1 || j == 0 || j == width + 1 || (i == 1 + height/2 && bariers)) //Wals and Bariers
            {
                mvprintw(startY + i, startX + j, "#");
            }
            else if (i == y + 1 && j == x + 1)
            {
                mvprintw(startY + i, startX + j, "O"); //Snake had
            }
            else if (i == fruitY + 1 && j == fruitX + 1) //Fruit
            {
                mvprintw(startY + i, startX + j, "F");    
            } 
            else //Tail
            { 
                bool print = false;
                for (int k = 0; k < nTail; k++) {
                    if (tailX[k] == j - 1 && tailY[k] == i - 1) // Adjust for tail position
                        mvprintw(startY + i, startX + j, "o");
                }
            }
        }
    }

    //Print text info
    mvprintw(startY + height + 3, startX, "Score: %d", score);
    mvprintw(startY + height + 4, startX, "Size of the area: width %d, height %d", width, height);
    refresh();
}

void Input() //Imput for running game
{
    keypad(stdscr, true);
    halfdelay(1);
    int ch = getch();

    switch (ch) 
    {
        case 'a':
            if (dir != 1) //Left
            {
                dir = 1;
            }
            break;
        case 'd':
            if (dir != 2) //Right
            {
                dir = 2;
            }
            break;
        case 'w':
            if (dir != 3) //Up
            {   
                dir = 3;
            }
            break;
        case 's':
            if (dir != 4) //Down
            {
                dir = 4;
            }
            break;
        case 'p': //Pause game switch
            if(gamePause)gamePause = false;
            else gamePause = true;
            break;
        case 'x': //End game
            gameOver = true;
            break;
    }
}

bool InputMenu() //Imput for main menu
{
    keypad(stdscr, true);
    halfdelay(1);
    int ch = getch();

    switch (ch) 
    {
        case '1': //Normal gamemode
            walls = false;
            bariers = false;
            return true;
        case '2': //Wall gamemode
            walls = true;
            bariers = false;
            return true;
        case '3': //Barier gamemode
            walls = false;
            bariers = true;
            return true;
        case 'x':
            gameOver = true;
            return true;
    }
    return false;
}

bool Logic() {
    if(nTail+1 == (width * height)) //Win game you fill all spaces
    {
        gameOver = true;
        return true;
    }
    else //Play game
    {
        int prevX = tailX[0];
        int prevY = tailY[0];
        int prev2X, prev2Y;
        tailX[0] = x;
        tailY[0] = y;

        for (int i = 1; i < nTail; i++) //Shift tail
        {
            prev2X = tailX[i];
            prev2Y = tailY[i];
            tailX[i] = prevX;
            tailY[i] = prevY;
            prevX = prev2X;
            prevY = prev2Y;
        }

        switch (dir) //Movement
        {
            case 1:
                x--;
                break;
            case 2:
                x++;
                break;
            case 3:
                y--;
                break;
            case 4:
                y++;
                break;
        }

        //Head hit walls
        if (x >= width) //Hit right wall
        {
            if(walls)
            {
                gameOver = true;
                return false;
            }
            else
            {
                x = 0; 
            }
        }
        else if (x < 0 ) //Hit left wall
        {
            if(walls)
            {
                gameOver = true;
                return false;
            }
            else
            {
                x = width - 1;
            }
        }
          
        if (y >= height) //Hit bottom wall
        {
            if(walls)
            {
                gameOver = true;
                return false;
            }
            else
            {
                y = 0;
            }
        }
        
        else if (y < 0) //Hit upper wall
        {
            if(walls)
            {
                gameOver = true;
                return false;
            }
            else
            {
                y = height - 1;
            }
        }

        if(y == height/2 && bariers) //Head hit barrier
        {
            gameOver = true;
            return false;
        }

        for (int i = 0; i < nTail; i++) //Head hit into body
        {
            if (tailX[i] == x && tailY[i] == y) gameOver = true;
        }

        if (x == fruitX && y == fruitY) //Head eat fruit
        {
            //Add score and tail
            score += 10;
            tailX[nTail] = x;
            tailY[nTail] = y;
            nTail++;

            //Generate new food
            fruitX = rand() % (width - 2) + 1;
            fruitY = rand() % (height - 2) + 1;
            bool control = true;

            //Avoid spawn fruit into wall and body
            while(control)
            {
                control = false;
                if(fruitX == x && fruitY == y)
                {
                    fruitX = rand() % (width - 2) + 1; 
                    fruitY = rand() % (height - 2) + 1; 
                    control = true;      
                }

                for(int i = 0; i < nTail; i++)
                {
                    if(tailX[i] == fruitX && tailY[i] == fruitY)
                    {
                        fruitX = rand() % (width - 2) + 1; 
                        fruitY = rand() % (height - 2) + 1; 
                        control = true;
                    }
                
                }
                if(1 + height/2 == fruitY && bariers)
                {
                    fruitX = rand() % (width - 2) + 1; 
                    fruitY = rand() % (height - 2) + 1; 
                    control = true;
                }
                
            }    
        }
        return false;
    }
}

void printMenu(int posX, int posY) //Print menu text
{
    clear();
    mvprintw(posY, posX-11, "Welcome in game Snake");
    mvprintw(posY+2, posX-32, "Controls of movement are: W - UP, S - DOWN, A - LEFT, D - RIGHT");
    mvprintw(posY+4, posX-25, "Pause the game  P, and for Exit the game X");
    mvprintw(posY+6, posX-30, "For normal game with no barriers and walls press 1");
    mvprintw(posY+7, posX-30, "For game with walls you can not go trough press 2");
    mvprintw(posY+8, posX-30, "For game with killing wall in the middle and passing sight walls press 3");
}

void gameRun(int posX, int posY)
{
    while (!gameOver) //Normaly runnig game in loop
    {
        if(gamePause) //If paused game 
        {
            clear();
            mvprintw(posY, posX-5, "Game paused");
            mvprintw(posY+1, posX-9, "Your score is: %d", score);

            Input(); //Wait for input
            usleep(100000); //Sleep 100 ms
        }
        else
        {
            Draw(); //Draw all data to console        
            Input(); //Read keyboard buttons
            win = Logic(); //Logic of snake game
            usleep(100000); //Sleep 100 ms
        }
    }
}

void printEndScreen(int posX, int posY)
{
    clear();
    cbreak();
    if(win) //Winner screen
    {
        mvprintw(posY, posX-11, "Game Over, but you won!");
        mvprintw(posY + 1, posX-25, "Your score is the highest possible in this area: %d", score);
    }
    else //Game over screen
    {
        mvprintw(posY, posX-5, "Game Over");
        mvprintw(posY + 1, posX-9, "Your score is: %d", score);
    }

    mvprintw(posY + 2, posX-9, "Thanks for playing");
    mvprintw(posY + 3, posX -12, "Press any key to continue");
    getch(); //wait for button 
}

int main() 
{
    //Main variables
    int mX, mY;

    srand(time(NULL)); //Set time for random 

    pair<int, int> rozmery = ran(); //Generate random size of gameboard
    width = rozmery.first;
    height = rozmery.second;

    //Set wait screan 
    initscr();
    noecho();
    curs_set(0);

    Setup(); //Setup for start new game 

    //Return size of linux console
    getmaxyx(stdscr, mY, mX);
    int posX = mX / 2;
    int posY = mY / 2;

    printMenu(posX, posY); //Print menu text
    while(!InputMenu()) usleep(10000); //Wait for press button in menu
    
    gameRun(posX, posY); //Play game 
    printEndScreen(posX, posY); //Print end screen in middle

    endwin(); //End close window
    return 0;
}
